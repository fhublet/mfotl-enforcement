
open MFOTL
open EMFOTL
open Predicate

   

(* Checking of relative past formulae *)
   
let min_bound a b =
  match a, b with
    Inf   , _
  | _     , Inf               -> Inf
  | OBnd a, OBnd b
  | OBnd a, CBnd b when a < b -> OBnd a
  | CBnd a, CBnd b
  | CBnd a, OBnd b when a <= b-> CBnd a
  | OBnd a, OBnd b
  | CBnd a, OBnd b when a > b -> OBnd b
  | OBnd a, CBnd b
  | CBnd a, CBnd b when a >= b-> CBnd b
  | _ -> assert false

let max_bound a b =
  match a, b with
    Inf   , _
  | _     , Inf               -> Inf
  | OBnd a, OBnd b
  | CBnd a, OBnd b when a < b -> OBnd b
  | OBnd a, CBnd b
  | CBnd a, CBnd b when a <= b-> CBnd b
  | OBnd a, OBnd b when a <= b-> OBnd b
  | OBnd a, OBnd b
  | OBnd a, CBnd b when a > b -> OBnd a
  | CBnd a, OBnd b
  | CBnd a, CBnd b when a >= b-> CBnd a
  | OBnd a, OBnd b when a >= b-> OBnd a
  | _ -> assert false
       

let sum_bound a b =
  match a, b with
    Inf   , _
  | _     , Inf               -> Inf
  | OBnd a, OBnd b            -> OBnd (a +. b)
  | CBnd a, OBnd b            -> OBnd (a +. b)
  | OBnd a, CBnd b            -> OBnd (a +. b)
  | CBnd a, CBnd b            -> CBnd (a +. b)

let inv_bound = function
    Inf    -> Inf
  | OBnd a -> OBnd (-. a)
  | CBnd a -> CBnd (-. a)

let union (l1, r1) (l2, r2) =
  (min_bound l1 l2, max_bound r1 r2)

let sum (l1, r1) (l2, r2) =
  (sum_bound l1 l2, sum_bound r1 r2)

let to_zero (_, r1) =
  (CBnd 0., r1)

let zero_in (_, r1) =
  match r1 with
    OBnd a -> a > 0.
  | CBnd a -> a >= 0.
  | _      -> true

let is_negative (_, r1) =
  match r1 with
    OBnd a
  | CBnd a -> a <= 0.
  | _      -> false

let inv (l1, r1) =
  (inv_bound r1, inv_bound l1)
       
let rec relative_interval = function
    Equal (_, _) 
  | Less (_, _)
  | LessEq (_, _)
  | Pred _                 -> (CBnd 0., CBnd 0.)
  | Neg f
  | Exists (_, f)
  | ForAll (_, f)
  | Aggreg (_, _, _, _, f) -> relative_interval f
  | And (f1, f2, _)
  | Or (f1, f2)
  | Implies (f1, f2)
  | Equiv (f1, f2)         -> union (relative_interval f1) (relative_interval f2)
  | Prev (i, f)
  | Once (i, f)
  | PastAlways (i, f)      -> union (to_zero (inv i)) (sum (inv i) (relative_interval f))
  | Next (i, f)
  | Eventually (i, f)
  | Always (i, f)          -> union (to_zero i) (sum i (relative_interval f))
  | Since (i, f1, f2)      -> union (to_zero (inv i))
                                    (union (sum (to_zero (inv i)) (relative_interval f1))
                                       (sum (inv i) (relative_interval f2)))
  | Until (i, f1, f2)      -> union (to_zero i)
                                    (union (sum (to_zero i) (relative_interval f1))
                                       (sum i (relative_interval f2)))

let strict f = 
  let rec _strict itv fut f =
    ((zero_in itv) && fut)
    || (match f with
          Equal (_, _) 
        | Less (_, _)
        | LessEq (_, _)
        | Pred _                 -> false
        | Neg f
        | Exists (_, f)
        | ForAll (_, f)
        | Aggreg (_, _, _, _, f) -> _strict itv fut f
        | And (f1, f2, _)
        | Or (f1, f2)
        | Implies (f1, f2)
        | Equiv (f1, f2)         -> (_strict itv fut f1) || (_strict itv fut f2)
        | Prev (i, f)
        | Once (i, f)
        | PastAlways (i, f)      -> _strict (sum (inv i) itv) fut f
        | Next (i, f)
        | Eventually (i, f)
        | Always (i, f)          -> _strict (sum i itv) true f
        | Since (i, f1, f2)      -> (_strict (sum (inv i) itv) fut f1)
                                    || (_strict (sum (inv i) itv) fut f2)
        | Until (i, f1, f2)      -> (_strict (sum i itv) true f1)
                                    || (_strict (sum i itv) true f2))
  in not (_strict (CBnd 0., CBnd 0.) false f)
  
let relative_past f =
  is_negative (relative_interval f)               
                
let strictly_relative_past f =
  (relative_past f) && (strict f)

(* Checking of SMFOTL *)

let get_levels (sch: Db.schema) =
  let tbl = Hashtbl.create 10 in
  List.iter (fun (s: Table.schema) -> Hashtbl.add tbl s.name s.level) sch;
  tbl

module MapS = Map.Make(String)
module SetS = Set.Make(String)
  
let rec _is_enforceable lvls (cc: Table.lvl MapS.t) = 
  function
    Pred p            -> begin
                           let pred = Predicate.get_name p in
                           match Hashtbl.find lvls pred with
                             Table.Con    -> true, cc, Pred p
                           | Table.ConCau -> (match MapS.find_opt pred cc with
                                                None             -> true, MapS.add pred Table.Con cc, Pred p
                                              | Some (Table.Con) -> true, cc, Pred p
                                              | Some _           -> false, cc, Pred p)
                           | _            -> false, cc, Pred p
                         end
  | Neg (Pred p)      -> begin
                           let pred = Predicate.get_name p in
                           match Hashtbl.find lvls (Predicate.get_name p) with
                           Table.Cau    -> true, cc, Neg (Pred p)
                         | Table.ConCau -> (match MapS.find_opt pred cc with
                                                None             -> true, MapS.add pred Table.Cau cc, Neg (Pred p)
                                              | Some (Table.Cau) -> true, cc, Neg (Pred p)
                                              | Some _           -> false, cc, Neg (Pred p))
                         | _            -> false, cc, Neg (Pred p)
                         end
  | Neg f             -> false, cc, Neg f (* see how we can work with the polarity *)
  | Exists (v, f)     -> let b, cc, f = _is_enforceable lvls cc f in
                         b, cc, Exists (v, f)
  | ForAll (v, f)     -> false, cc, ForAll (v, f) (* ? *)
  (* \forall x. \varphi ::= \not \exists x. \not \varphi ? *)
  | And (f1, f2, _)   -> begin
                           match _is_enforceable lvls cc f1 with
                             true, cc, f1 -> true, cc, And (f1, f2, MFOTL.Left)
                           | false, _, _  -> (match _is_enforceable lvls cc f2 with
                                                true, cc, f2
                                                -> true, cc, And (f1, f2, MFOTL.Right)
                                              | false, _, _
                                                -> false, cc, And (f1, f2, MFOTL.Neither))
                         end
  | Or (f1, f2)       -> let b1, cc, f1 = _is_enforceable lvls cc f1 in
                         let b2, cc, f2 = _is_enforceable lvls cc f2 in
                         b1 && b2, cc, Or (f1, f2)
  | Implies (f1, f2)  -> false, cc, Implies (f1, f2) (*(_is_enforceable lvls (not pol) f1) && (_is_enforceable lvls f2)*)
                         (* f1 => f2 ::= not f1 or f2 *)
  | Equiv (f1, f2)    -> false, cc, Equiv (f1, f2)
  | PastAlways (i, f) -> false, cc, PastAlways (i, f) (*(zero_in i) && (_is_enforceable lvls f)*)
  | Since (i, f1, f2) -> false, cc, Since (i, f1, f2) (*(zero_in i) && (_is_enforceable lvls f1) && (_is_enforceable lvls f2)*)
  | f                 -> false, cc, f

let is_enforceable s f =
  let l = get_levels s in
  let (b, l, v) = _is_enforceable l MapS.empty f in
  let s = List.map (fun db -> Table.{ db with level = match MapS.find_opt db.name l with
                                                        None -> db.level
                                                      | Some l -> l }) s in
  b, s, v

(* Countermeasures *)

module PredSet = Set.Make(
  struct
    type t = predicate
    let compare = compare
  end)

let print_predset str s =
  print_string str;
  PredSet.iter (fun pred -> Predicate.print_predicate pred; print_string " ") s;
  print_newline ()

(* Save state and restore *)

let rec save = function
    EPred (_,_,inf)   -> inf.oldrels <- Queue.copy inf.rels
  | ERel _            -> ()
  | ENeg (f1,_)       -> save f1
  | EAnd (_,f1,f2,_, _)                        (* no need for arel in enforcement mode since formulae are strictly relative-past *)
  | EOr (_,f1,f2,_)   -> (save f1; save f2) (* no need for arel in enforcement mode since formulae are strictly relative-past *)
  | EExists (_,f1,_)  -> save f1
  | EAggreg (_,_,f1)
  | EAggOnce (_,_,f1) -> save f1
  | EPrev (_,f1,inf)  -> (inf.oldplast <- Some inf.plast; save f1)
  | ENext (_,f1,inf)  -> (inf.oldinit <- inf.init; save f1)
  | EOnceA (_,f1,inf) -> (inf.oldores <- Some inf.ores;
                          inf.oldoaauxrels <- Mqueue.copy inf.oaauxrels;
                          save f1)
  | EOnceZ (_,f1,inf) -> (inf.oldoztree <- Some inf.oztree;
                          let oozr, oozl = Dllist.copy inf.ozauxrels inf.ozlast in
                          inf.oldozlast <- Some oozl;
                          inf.oldozauxrels <- oozr;
                          save f1)
  | EOnce (_,f1,inf)  -> (inf.oldotree <- Some inf.otree;
                          let oar, ool = Dllist.copy inf.oauxrels inf.olast in
                          inf.oldolast <- Some ool;
                          inf.oldoauxrels <- oar;
                          save f1)
  | EEventuallyZ (_,f1,inf) -> (inf.oldeztree <- Some inf.eztree;
                                let oezr, oezl = Dllist.copy inf.ezauxrels inf.ezlast in
                                inf.oldezlast <- Some oezl;
                                inf.oldezlastev <- Some inf.ezlastev;
                                inf.oldezauxrels <- oezr;
                                save f1)
  | EEventually (_,f1,inf) -> (inf.oldetree <- Some inf.etree;
                               let oer, oel = Dllist.copy inf.eauxrels inf.elast in
                               inf.oldelast <- Some oel;
                               inf.oldelastev <- Some inf.elastev;
                               inf.oldeauxrels <- oer;
                               save f1)
  | ESinceA (_,_,f1,f2,inf) -> (inf.oldsres <- Some inf.sres;
                                inf.oldsaauxrels <- Mqueue.copy inf.saauxrels;
                                save f1; save f2)
  | ESince (_,_,f1,f2,inf) -> (inf.oldsauxrels <- Mqueue.copy inf.sauxrels;
                               save f1; save f2)
  | EUntil (_,_,f1,f2,inf) -> (inf.oldulast <- Some inf.ulast;
                               inf.oldufirst <- inf.ufirst;
                               inf.oldures  <- Some inf.ures;
                               inf.oldraux <- fst (Sj.copy inf.raux Sj.void);
                               inf.oldsaux <- fst (Sk.copy inf.saux Sk.void);
                               save f1; save f2)
  | ENUntil (_,_,f1,f2,inf) -> (inf.oldlast1 <- Some inf.last1;
                                inf.oldlast2 <- Some inf.last2;
                                inf.oldlistrel1 <- fst (Sj.copy inf.listrel1 Sj.void);
                                inf.oldlistrel2 <- fst (Sk.copy inf.listrel2 Sk.void);
                                save f1; save f2)
  
let rec undo = function
    EPred (_,_,inf)   -> inf.rels <- Queue.copy inf.oldrels
  | ERel _            -> ()
  | ENeg (f1,_)       -> undo f1
  | EAnd (_,f1,f2,_,_)                        (* no need for arel in enforcement mode since formulae are strictly relative-past *)
  | EOr (_,f1,f2,_)   -> (undo f1; undo f2) (* no need for arel in enforcement mode since formulae are strictly relative-past *)
  | EExists (_,f1,_)  -> undo f1
  | EAggreg (_,_,f1)
  | EAggOnce (_,_,f1) -> undo f1
  | EPrev (_,f1,inf)    -> ((match inf.oldplast with
                              None -> assert false
                             | Some opl -> inf.plast <- opl); 
                            undo f1)
  | ENext (_,f1,inf)  -> (inf.init <- inf.oldinit; undo f1)
  | EOnceA (_,f1,inf) -> ((match inf.oldores with
                            None -> assert false
                           | Some oor -> inf.ores <- oor);
                          inf.oaauxrels <- inf.oldoaauxrels;
                          undo f1)
  | EOnceZ (_,f1,inf) -> ((match inf.oldoztree, inf.oldozlast with
                             Some oot, Some ool -> (inf.oztree <- oot; inf.ozlast <- ool)
                           | _       , _        -> assert false);
                          inf.ozauxrels <- inf.oldozauxrels;
                          undo f1)
  | EOnce (_,f1,inf)  -> ((match inf.oldotree, inf.oldolast with
                             Some oot, Some ool -> (inf.otree <- oot; inf.olast <- ool)
                           | _       , _        -> assert false);
                          inf.oauxrels <- inf.oldoauxrels;
                          undo f1)
  | EEventuallyZ (_,f1,inf) -> ((match inf.oldeztree, inf.oldezlast, inf.oldezlastev with
                                   Some oet, Some oel, Some oele ->
                                    (inf.eztree <- oet; inf.ezlast <- oel; inf.ezlastev <- oele)
                                 | _       , _       , _        -> assert false);
                                inf.ezauxrels <- inf.oldezauxrels;
                                undo f1)
  | EEventually (_,f1,inf) -> ((match inf.oldetree, inf.oldelast, inf.oldelastev with
                                  Some oet, Some oel, Some oele ->
                                   (inf.etree <- oet; inf.elast <- oel; inf.elastev <- oele)
                                | _       , _       , _        -> assert false);
                               inf.eauxrels <- inf.oldeauxrels;
                                undo f1)
  | ESinceA (_,_,f1,f2,inf) -> ((match inf.oldsres with
                                   Some osr -> inf.sres <- osr
                                 | _        -> assert false);
                                inf.saauxrels <- inf.oldsaauxrels;
                                undo f1; undo f2)
  | ESince (_,_,f1,f2,inf) -> (inf.sauxrels <- inf.oldsauxrels;
                               undo f1; undo f2)
  | EUntil (_,_,f1,f2,inf) -> ((match inf.oldulast, inf.oldures with
                                   Some oul, Some our -> (inf.ulast <- oul; inf.ures <- our)
                                 | _       , _        -> assert false);
                               inf.ufirst <- inf.oldufirst;
                               inf.raux <- inf.oldraux;
                               inf.saux <- inf.oldsaux;
                                undo f1; undo f2)
  | ENUntil (_,_,f1,f2,inf) -> ((match inf.oldlast1, inf.oldlast2 with
                                   Some ol1, Some ol2 -> (inf.last1 <- ol1; inf.last2 <- ol2)
                                 | _       , _        -> assert false);
                                inf.listrel1 <- inf.oldlistrel1;
                                inf.listrel2 <- inf.oldlistrel2;
                                undo f1; undo f2)

(* Modify db *)

let update_db dbschema db block trigger =
  (*print_predset "Block: " block;
  print_predset "Trigger: " trigger;
  print_string "Tables before: ";
  List.iter Table.print_table (Db.get_tables db); print_newline();*)
  let tables = Db.get_tables db in
  (* names of current tables *)
  let cur_tables = List.fold_left (fun s x -> SetS.add (Table.get_schema x).name s)
                     SetS.empty tables in
  let new_tables = PredSet.fold (fun p s -> SetS.add (Predicate.get_name p) s)
                     (PredSet.union block trigger) SetS.empty in
  let diff_tables = SetS.diff new_tables cur_tables in
  (* add empty tables for all tables with name in diff_tables *)
  let tables = SetS.fold (fun name tables ->
                   (Table.make_table (Db.get_table_schema dbschema name) Relation.empty)::tables)
                 diff_tables tables in
  (* sort predicates to be blocked by name *)
  let block_p = PredSet.fold (fun p m -> MapS.update (Predicate.get_name p)
                                           (function None   -> Some (PredSet.singleton p)
                                                   | Some q -> Some (PredSet.add p q)) m)
                  block MapS.empty in
  (* same for predicates to be triggered *)
  let trigger_p = PredSet.fold (fun p m -> MapS.update (Predicate.get_name p)
                                             (function None   -> Some (PredSet.singleton p)
                                                     | Some q -> Some (PredSet.add p q)) m)
                    trigger MapS.empty in
  (* now, for each table, add or remove tuples *)
  let aux table =
    let (schema, relation) = Table.get_schema table, Table.get_relation table in
    let name = schema.name in
    let toblock = match MapS.find_opt name block_p with
        None -> []
      | Some s -> List.map Tuple.of_const_predicate (PredSet.elements s) in
    let totrigger = match MapS.find_opt name trigger_p with
        None -> []
      | Some s -> List.map Tuple.of_const_predicate (PredSet.elements s) in
    let relation = List.fold_right Relation.add totrigger relation in
    let relation = List.fold_right Relation.remove toblock relation in
    Table.make_table schema relation
  in
  let tables = List.map aux tables in
  (* remove empty tables *)
  let tables = List.filter (fun x -> not (Relation.is_empty (Table.get_relation x))) tables in
  (*print_string "Tables after: ";
  List.iter Table.print_table tables; print_newline();*)
  Db.make_db tables
    
             
