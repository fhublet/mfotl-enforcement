open Predicate
open MFOTL
open Relation

module Sk = Dllist
module Sj = Dllist

type info = {mutable rels: (int * timestamp * relation) Queue.t;
             mutable last: relation option;
             mutable oldrels: (int * timestamp * relation) Queue.t;
             pos: int list}
type ainfo = {mutable arel: relation option;
              mutable last: relation option;
              proj: Tuple.tuple -> Tuple.tuple;
              proj2: (Tuple.tuple -> Tuple.tuple) option;
              pos: int list}
type pinfo = {mutable plast: Neval.cell;
              mutable last: relation option;
              mutable oldplast: Neval.cell option}
type ninfo = {mutable init: bool;
              mutable last: relation option;
              mutable oldinit: bool}
type linfo = {mutable last: relation option;
              pos: int list}
type oainfo = {mutable ores: relation;
               mutable oaauxrels: (timestamp * relation) Mqueue.t;
               mutable last: relation option;
               mutable oldores: relation option;
               mutable oldoaauxrels: (timestamp * relation) Mqueue.t}
type agg_info = {agg_op: agg_op;
                 agg_default: cst option;
                 mutable last: relation option}
type ozinfo = {mutable oztree: (int, relation) Sliding.stree;
               mutable ozlast: (int * timestamp * relation) Dllist.cell;
               mutable ozauxrels: (int * timestamp * relation) Dllist.dllist;
               mutable last: relation option;
               mutable oldoztree: (int, relation) Sliding.stree option;
               mutable oldozlast: (int * timestamp * relation) Dllist.cell option;
               mutable oldozauxrels: (int * timestamp * relation) Dllist.dllist}
type oinfo = {mutable otree: (timestamp, relation) Sliding.stree;
              mutable olast: (timestamp * relation) Dllist.cell;
              mutable oauxrels: (timestamp * relation) Dllist.dllist;
              mutable last: relation option;
              mutable oldotree: (timestamp, relation) Sliding.stree option;
              mutable oldolast: (timestamp * relation) Dllist.cell option;
              mutable oldoauxrels: (timestamp * relation) Dllist.dllist}
type sainfo = {mutable sres: relation;
               mutable sarel2: relation option;
               mutable saauxrels: (timestamp * relation) Mqueue.t;
               mutable last: relation option;
               mutable oldsres: relation option;
               mutable oldsaauxrels: (timestamp * relation) Mqueue.t}
type sinfo = {mutable srel2: relation option;
              mutable sauxrels: (timestamp * relation) Mqueue.t;
              mutable last: relation option;
              mutable oldsauxrels: (timestamp * relation) Mqueue.t}
type ezinfo = {mutable ezlastev: Neval.cell;
               mutable eztree: (int, relation) Sliding.stree;
               mutable ezlast: (int * timestamp * relation) Dllist.cell;
               mutable ezauxrels: (int * timestamp * relation) Dllist.dllist;
               mutable last: relation option;
               mutable oldezlastev: Neval.cell option;
               mutable oldeztree: (int, relation) Sliding.stree option; 
               mutable oldezlast: (int * timestamp * relation) Dllist.cell option;
               mutable oldezauxrels: (int * timestamp * relation) Dllist.dllist}
type einfo = {mutable elastev: Neval.cell;
              mutable etree: (timestamp, relation) Sliding.stree;
              mutable elast: (timestamp * relation) Dllist.cell;
              mutable eauxrels: (timestamp * relation) Dllist.dllist;
              mutable last: relation option;
              mutable oldelastev: Neval.cell option;
              mutable oldetree: (timestamp, relation) Sliding.stree option;
              mutable oldelast: (timestamp * relation) Dllist.cell option;
              mutable oldeauxrels: (timestamp * relation) Dllist.dllist}
type uinfo = {mutable ulast: Neval.cell;
              mutable ufirst: bool;
              mutable ures: relation;
              mutable urel2: relation option;
              mutable raux: (int * timestamp * (int * relation) Sk.dllist) Sj.dllist;
              mutable saux: (int * relation) Sk.dllist;
              mutable last: relation option;
              mutable oldulast: Neval.cell option;
              mutable oldufirst: bool;
              mutable oldures: relation option;
              mutable oldraux: (int * timestamp * (int * relation) Sk.dllist) Sj.dllist;
              mutable oldsaux: (int * relation) Sk.dllist}
type uninfo = {mutable last1: Neval.cell;
               mutable last2: Neval.cell;
               mutable listrel1: (int * timestamp * relation) Dllist.dllist;
               mutable listrel2: (int * timestamp * relation) Dllist.dllist;
               mutable last: relation option;
               mutable oldlast1: Neval.cell option;
               mutable oldlast2: Neval.cell option;
               mutable oldlistrel1: (int * timestamp * relation) Dllist.dllist;
               mutable oldlistrel2: (int * timestamp * relation) Dllist.dllist;}

type comp_one = relation -> relation
type comp_two = relation -> relation -> relation

type extformula =
  | ERel of relation
  | EPred of predicate * comp_one * info
  | ENeg of extformula * linfo
  | EAnd of comp_two * extformula * extformula * ainfo * enf_flag
  | EOr of comp_two * extformula * extformula * ainfo
  | EExists of comp_one * extformula * linfo
  | EAggreg of agg_info * Aggreg.aggregator * extformula
  | EAggOnce of agg_info * Aggreg.window_aggregator * extformula
  | EPrev of interval * extformula * pinfo
  | ENext of interval * extformula * ninfo
  | ESinceA of comp_two * interval * extformula * extformula * sainfo
  | ESince of comp_two * interval * extformula * extformula * sinfo
  | EOnceA of interval * extformula * oainfo
  | EOnceZ of interval * extformula * ozinfo
  | EOnce of interval * extformula * oinfo
  | ENUntil of comp_two * interval * extformula * extformula * uninfo
  | EUntil of comp_two * interval * extformula * extformula * uinfo
  | EEventuallyZ of interval * extformula * ezinfo
  | EEventually of interval * extformula * einfo

val get_last: extformula -> relation option
val get_pos: extformula -> int list

val print_extf: var -> extformula -> unit
val print_auxel: int * relation -> unit
val print_sauxel: timestamp * relation -> unit
val print_uinf: var -> uinfo -> unit
val print_predinf: var -> (int * timestamp * relation) Queue.t -> unit
val print_ezinf: var -> ezinfo -> unit
val print_einf: var -> einfo -> unit
val print_einfn: var -> einfo -> unit
val print_oinf: var -> oinfo -> unit
