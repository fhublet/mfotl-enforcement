open MFOTL
open EMFOTL
open Predicate

val strictly_relative_past: formula -> bool

val get_levels: Db.schema -> (var, Table.lvl) Hashtbl.t
  
val is_enforceable: Db.schema -> formula -> bool * Db.schema * formula

module PredSet: Set.S with type elt = Predicate.predicate
               
val print_predset: string -> PredSet.t -> unit

val save: extformula -> unit

val undo: extformula -> unit

val update_db: Db.schema -> Db.db -> PredSet.t -> PredSet.t -> Db.db
                          
